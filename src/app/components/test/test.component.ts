import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit() {
    console.log(this.router.url);
  }

  storeData() {
    localStorage.setItem(`token`, `_eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9`);
    console.log("Added!", `token`);
  }
  logData() {
    console.log(`YOUR TOKEN: `, localStorage.getItem(`token`));
  }
  clearData() {
    localStorage.removeItem(`token`);
    console.log("Removed!", `token`);
  }

}
