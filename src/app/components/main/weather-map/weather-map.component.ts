import { Component, OnInit, Input, OnChanges } from '@angular/core';
import postscribe from 'postscribe';

@Component({
    selector: 'app-weather-map',
    templateUrl: './weather-map.component.html',
    styleUrls: ['./weather-map.component.scss']
})
export class WeatherMapComponent implements OnInit, OnChanges {
    @Input() lat;
    @Input() long;
    // link = 'https://darksky.net/map-embed/@radar,' + "18.771" + ',' + "105.820" + ',10.js?embed=true&timeControl=false&fieldControl=false&defaultField=radar';
    // link = "https://maps.darksky.net/@temperature,18.771,105.820,5"
    // link = `https://darksky.net/map-embed/@temperature,42.213,-82.134,5.js?embed=true&timeControl=false&fieldControl=false&defaultField=temperature&defaultUnits=_c`;


    constructor() { }

    ngOnInit() {

    }

    ngOnChanges(change) {
        if (this.lat && this.long) {
            const resetElement = document.getElementById("darkSky");
            resetElement.innerHTML = "";
            const link = `https://darksky.net/map-embed/@temperature,${this.lat},${this.long},5.js?embed=true&timeControl=true&fieldControl=true&defaultField=temperature&defaultUnits=_c`;
            const script = '<script src="' + link + '"></script>';
            postscribe('#darkSky', script);
        }
    }


}
