import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { env } from "../../../../../config.dev";
import { Observable } from 'rxjs';
import { MapService } from 'src/app/services/map.service';
import { Router } from '@angular/router';
@Component({
    selector: 'app-recommendation',
    templateUrl: './recommendation.component.html',
    styleUrls: ['./recommendation.component.scss']
})
export class RecommendationComponent implements OnInit, OnChanges {

    @Input() placeInfo;

    constructor(private mapService: MapService, private router: Router) { }

    ngOnInit() {
    }

    ngOnChanges(change) {
    }
}

