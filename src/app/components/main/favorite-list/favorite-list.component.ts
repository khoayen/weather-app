import { Observable, of } from 'rxjs';
import { PlaceService } from './../../../services/place.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Place } from 'src/app/interfaces/place';
import { ToastrService } from 'ngx-toastr';
@Component({
    selector: 'app-favorite-list',
    templateUrl: './favorite-list.component.html',
    styleUrls: ['./favorite-list.component.scss']
})
export class FavoriteListComponent implements OnInit {

    favoritePlaces$: Observable<Place[]>;
    loading = false;
    constructor(private toastr: ToastrService, private placeService: PlaceService, private router: Router) { }

    ngOnInit() {
        this.getPlaces();
    }

    getPlaces() {
        this.loading = true;
        this.placeService.getFavoritePlaces().subscribe(
            (response: any) => {
                if (response.statusCode === 200) {
                    this.favoritePlaces$ = of(response.result);
                    this.loading = false;
                } else {
                    this.toastr.error(response.error);
                    this.router.navigate(['error']);
                }
            },
            error => {
                this.router.navigate(['error']);
                // https://stackoverflow.com/questions/49144888/angular-5-commands-reduce-is-not-a-function-in-router-navigate
            }
        );
    }

}
