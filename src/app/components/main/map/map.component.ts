import { Place } from './../../../interfaces/place';
import { Marker } from './../../../interfaces/marker';
import { Component, OnInit, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { PlaceService } from 'src/app/services/place.service';

// import { setTimeout } from 'timers';

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnChanges {
    constructor(private toastr: ToastrService, private placeService: PlaceService) {

    }

    @Input() placeInfo;

    marker: Marker;
    @Input() isMarkerDraggable = true;

    @Output() changeLocation = new EventEmitter<any>();

    ngOnChanges(change) {
        if (change.placeInfo) {
            this.placeInfo = { ...change.placeInfo.currentValue };
        }

        if (this.placeInfo) {
            this.marker = {
                lat: this.placeInfo.lat,
                long: this.placeInfo.long,
                draggable: this.isMarkerDraggable
            } as Marker;
        }

        // console.log("map - 36", this.placeInfo)
    }

    ngOnInit() {
    }

    markerDragEnd($event) {
        // console.log($event.coords.lat);
        // console.log($event)
        const lat = $event.coords.lat;
        const long = $event.coords.lng;
        this.changeLocation.emit({ lat, long });
    }
}
