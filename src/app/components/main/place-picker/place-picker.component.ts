import { MapService } from "./../../../services/map.service";
import { Place } from "src/app/interfaces/place";
import { PlaceService } from "./../../../services/place.service";
import { WeatherService } from "./../../../services/weather.service";
import {
  Component,
  OnInit,
  ViewChild,
  ViewChildren,
  Output,
  EventEmitter,
  OnChanges
} from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-place-picker",
  templateUrl: "./place-picker.component.html",
  styleUrls: ["./place-picker.component.scss"]
})
export class PlacePickerComponent implements OnInit, OnChanges {
  placeInfo: Place;
  isButtonAvailable = true;

  @ViewChildren("placesRef")
  public placesRef: GooglePlaceDirective;

  constructor(
    private toastr: ToastrService,
    private mapService: MapService,
    private weatherService: WeatherService,
    private placeService: PlaceService
  ) { }

  changeLatLong($event) {
    // console.log("Test event", $event)
    this.placeInfo.lat = $event.lat;
    this.placeInfo.long = $event.long;

    const searchBox = document.getElementById("placeSearchBox");
    (searchBox as HTMLInputElement).value = `${$event.lat}, ${$event.long}`;

    this.isButtonAvailable = true;
    // console.log(this.placeInfo)
    // this.placeInfo = {
    //     lat: $event.lat,
    //     long: $event.long
    // }

    this.mapService
      .getPlaceInfo(this.placeInfo.lat, this.placeInfo.long)
      .subscribe(
        (info: any) => {
          this.placeInfo = {
            lat: this.placeInfo.lat,
            long: this.placeInfo.long
          };

          if (info.results[0]) {
            this.placeInfo.address = info.results[0].formatted_address;
          }
        },
        error => {
          this.toastr.error("Error");
        }
      );
  }

  async ngOnInit() {
    // this.placeInfo = { lat: 40.7579747, long: -73.98554 };
    // find browser location info
    try {
      const browserLocationInfo: any = await this.getBrowserLocation();
      this.placeInfo = {
        lat: browserLocationInfo.lat,
        long: browserLocationInfo.long
      } as Place;

      this.mapService
        .getPlaceInfo(this.placeInfo.lat, this.placeInfo.long)
        .subscribe(
          (info: any) => {
            this.placeInfo = {
              ...this.placeInfo,
              address: info.results[0].formatted_address
            };
          },
          error => {
            this.toastr.error("Error");
          }
        );
    } catch (error) {
      // console.log("place picker -47", this.placeInfo)
      this.toastr.error(error.toString());
    }
  }

  ngOnChanges(change) {
    // console.log("place picker - 52", change)
  }

  addFavorite() {
    // console.log("map - 43", this.placeInfo);

    const favoritePlace: Place = {
      lat: this.placeInfo.lat,
      long: this.placeInfo.long,
      name: this.placeInfo.name,
      address: this.placeInfo.address,
      rating: this.placeInfo.rating,
      phoneNumber: this.placeInfo.phoneNumber
    };

    // console.log("place-picker - 66", favoritePlace)
    this.placeService.addFavoritePlace(favoritePlace).subscribe(
      (response: any) => {
        if (response.statusCode === 201) {
          this.isButtonAvailable = false;
          this.toastr.success("Navigate to list to see!");
        } else {
          this.toastr.error(response.error);
        }
      },
      error => {
        if (error.error.statusCode === 412) {
          this.toastr.error(error.error.error);
        } else {
          this.toastr.error("Server error");
        }
      }
    );
  }

  public handleAddressChange(place) {
    // console.log("PLACE -55", place);
    const lat = place.geometry.location.lat();
    const long = place.geometry.location.lng();
    this.placeInfo = {
      lat,
      long,
      name: place.name,
      address: place.formatted_address,
      phoneNumber: place.formatted_phone_number,
      rating: place.rating
    };

    this.isButtonAvailable = true;
    // console.log("PLACE PICKER_65", this.placeInfo);
  }

  getBrowserLocation() {
    return new Promise((resolve, reject) => {
      if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(
          position => {
            resolve({
              lat: position.coords.latitude,
              long: position.coords.longitude
            });
          },
          errorMessage => {
            reject(`An error has occured while retrieving location`);
          }
        );
      } else {
        reject("Geolocation is not enabled on this browser");
      }
    });
  }
}
