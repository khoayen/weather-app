import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoritePlaceDetailComponent } from './favorite-place-detail.component';

describe('FavoritePlaceDetailComponent', () => {
  let component: FavoritePlaceDetailComponent;
  let fixture: ComponentFixture<FavoritePlaceDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoritePlaceDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoritePlaceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
