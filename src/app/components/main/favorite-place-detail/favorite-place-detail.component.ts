import { Place } from './../../../interfaces/place';
import { PlaceService } from './../../../services/place.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-favorite-place-detail',
    templateUrl: './favorite-place-detail.component.html',
    styleUrls: ['./favorite-place-detail.component.scss']
})
export class FavoritePlaceDetailComponent implements OnInit, OnDestroy {

    place$: Observable<Place>;
    placeLocation;
    isNotFound = false;
    loading = false;
    placeId;

    constructor(private toastr: ToastrService, private placeService: PlaceService, private route: ActivatedRoute, private router: Router) {
        const id = this.route.snapshot.paramMap.get("id");
        this.placeId = id;
        this.loading = true;


        this.placeService.getFavoritePlaceDetail(id).subscribe((response: any) => {
            if (response.statusCode === 200) {
                this.place$ = of(response.result);
                this.loading = false;
            } else {
                this.toastr.error(response.error);
                this.router.navigate(['error']);
            }

            // this.placeLocation = {
            //   lat: data.lat,
            //   long: data.long
            // };

        },
            error => {
                this.loading = false;
                if (error.status === 404) {
                    this.isNotFound = true;
                }
            });
    }

    ngOnInit() {
    }

    removeFromList() {

        // console.log(this.placeId)
        this.placeService.removeFavoritePlace(this.placeId)
            .subscribe((response: any) => {
                this.toastr.success("Remove success");
                this.router.navigate(["/favorite"]);
            },
                (error) => {
                    this.toastr.error("Server error");
                });
    }

    ngOnDestroy() {
        this.loading = false;
    }

}
