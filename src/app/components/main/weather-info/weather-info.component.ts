import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { WeatherService } from 'src/app/services/weather.service';

@Component({
  selector: 'app-weather-info',
  templateUrl: './weather-info.component.html',
  styleUrls: ['./weather-info.component.scss']
})
export class WeatherInfoComponent implements OnInit, OnChanges {
  weatherInfo;
  @Input() lat;
  @Input() long;
  constructor(private weatherService: WeatherService) { }

  ngOnInit() {
  }

  ngOnChanges(change) {
    this.getWeatherInfo();
    // console.log("weather info - 20", change)
  }


  getWeatherInfo() {
    this.weatherService.getWeather(this.lat, this.long)
      .subscribe((data: any) => {
        this.weatherInfo = data.main;
        // console.log("weather info - 28 ", this.weatherInfo)
      });

  }


}
