import { FavoritePlaceDetailComponent } from './components/main/favorite-place-detail/favorite-place-detail.component';
import { TestComponent } from './components/test/test.component';
import { LoginComponent } from 'src/app/components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from 'src/app/components/not-found/not-found.component';
import { PlacePickerComponent } from 'src/app/components/main/place-picker/place-picker.component';
import { FavoriteListComponent } from 'src/app/components/main/favorite-list/favorite-list.component';
import { ServerErrorComponent } from 'src/app/components/server-error/server-error.component';


const routes: Routes = [{
    path: "", component: HomeComponent
},
{
    path: "login", component: LoginComponent
},
{
    path: "favorite", component: FavoriteListComponent
},
{
    path: "favorite/:id", component: FavoritePlaceDetailComponent
},
{
    path: "error", component: ServerErrorComponent
}
    ,
{
    path: "a/:id1/b/:id2", component: TestComponent
},
{
    path: 'me',
    redirectTo: '/login',
    pathMatch: "full"
},
{
    path: "**", component: NotFoundComponent
}];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
