export interface Place {
    lat: number;
    long: number;
    name?: string;
    address?: string;
    phoneNumber?: string;
    rating?: number;
    createdAt?: Date;
    updatedAt?: Date;
}

