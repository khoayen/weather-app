import { Place } from './../interfaces/place';
import { env } from './../../../config.dev';
import { HttpService } from './http.service';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})


export class PlaceService {
  url: string;
  constructor(private httpService: HttpService) {
    this.url = `${env.url}/place`;
  }
  getFavoritePlaces(): Observable<Place[]> {
    return this.httpService.http.get<Place[]>(this.url, httpOptions);
  }
  getFavoritePlaceDetail(id): Observable<Place> {
    // console.log("ID: ", id);
    // console.log(`${this.url}/${id}`);
    return this.httpService.http.get<Place>(`${this.url}/${id}`, httpOptions);
  }

  addFavoritePlace(place): Observable<Place> {
    return this.httpService.http.post<Place>(`${this.url}`, place, httpOptions);
  }

  removeFavoritePlace(placeId): Observable<Place> {
    return this.httpService.http.delete<Place>(`${this.url}/${placeId}`, httpOptions);
  }
}
