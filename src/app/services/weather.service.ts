import { Observable } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';
import { Injectable } from '@angular/core';
import { env } from 'config.dev';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  key = env.weatherKey;
  constructor(private httpService: HttpService) {

  }

  getWeather(lat, long) {
    const url = `${env.weatherUrl}lat=${lat}&lon=${long}&units=metric&appid=${env.weatherKey}`;
    return this.httpService.http.get<Observable<any>>(url);
  }
}
