import { Observable } from 'rxjs/internal/Observable';
import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { env } from "../../../config.dev";

@Injectable({
    providedIn: 'root'
})
export class MapService {
    placeDetailsUrl = env.placeDetailsUrl;

    constructor(private httpService: HttpService) { }

    getPlaceInfo(lat, long) {
        const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${long}&key=${env.googleApiKey}`;

        return this.httpService.http.get<Observable<any>>(url, this.httpService.httpOptions);
    }

}
