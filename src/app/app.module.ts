import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from 'src/app/components/login/login.component';
import { PlacePickerComponent } from './components/main/place-picker/place-picker.component';
import { MapComponent } from './components/main/map/map.component';
import { FormsModule } from '@angular/forms';
import { RecommendationComponent } from './components/main/recommendation/recommendation.component';
import { TestComponent } from './components/test/test.component';
import { FavoriteListComponent } from './components/main/favorite-list/favorite-list.component';
import { ErrorComponent } from './components/error/error.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ServerErrorComponent } from './components/server-error/server-error.component';
import { FavoritePlaceDetailComponent } from './components/main/favorite-place-detail/favorite-place-detail.component';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import { AgmCoreModule } from '@agm/core';
import { env } from "../../config.dev";
import { WeatherInfoComponent } from './components/main/weather-info/weather-info.component';

import { ToastrModule } from 'ngx-toastr';
import { WeatherMapComponent } from './components/main/weather-map/weather-map.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    PlacePickerComponent,
    MapComponent,
    RecommendationComponent,
    TestComponent,
    FavoriteListComponent,
    ErrorComponent,
    NotFoundComponent,
    ServerErrorComponent,
    FavoritePlaceDetailComponent,
    WeatherInfoComponent,
    WeatherMapComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    GooglePlaceModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxLoadingModule.forRoot({
      fullScreenBackdrop: true,
      animationType: ngxLoadingAnimationTypes.threeBounce
    }),
    AgmCoreModule.forRoot({
      apiKey: env.googleApiKey
    }),
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
